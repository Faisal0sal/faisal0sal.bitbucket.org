Monster = function(_game, _x, _y)
{
  console.log("Creating Monster!");
  
	this.game = _game;
	  
	Phaser.Sprite.call(this, this.game, _x, _y, 'Monster');
	  
	this.game.add.existing(this);
  
    this.enableBody = true;
    this.game.physics.arcade.enable([this]);
    this.enableBody = true;
	this.anchor.setTo(0,0);
	this.collideWorldBounds = true;
	this.checkWorldBounds = true;
	this.outOfBoundsKill = true;
	this.width = 280;
	this.height = 320;
	this.health = 20;
  
	
  // ------------------------------- Animation
  this.animations.add('breath',[0,1,2,3,4,5,6,7,8,9],8, true);
  this.animations.play('breath');
  
  	
  // --------------------------------- Play sound
  this.events.onKilled.add(function(){
		this.RestartMenu();
		//die.play();
	}, this);
	
	Monsterhealth = this.game.add.text(this.x, this.y, "Health " + this.health, { font: '16px Arial', fill: '#ffffff' });
	
  return this;
};

Monster.prototype = Object.create(Phaser.Sprite.prototype);
Monster.prototype.constructor = Monster;

Monster.prototype.update = function() {
	
	Monsterhealth.y = this.y;
	Monsterhealth.x = this.x + (this.width / 3);
	
};

Monster.prototype.RestartMenu = function() {
	
	label = this.game.add.text(Monster.x -30 , this.game.height / 2, '',{ font: '22px Lucida Console', fill: '#fff', align: 'center'});
	label.text ='\nGood Job\nYou Finished the first level';
	label.anchor.setTo(0.5, 0.5);
	label.fixedToCamera = true;
    label.cameraOffset.setTo(this.game.camera.width / 2, this.game.camera.height / 2);
	this.game.world.remove(Monsterhealth);
    MinionGenerator.timer.stop();
};

Monster.prototype.Fire = function() {
	
/*
		for(var i=0;i < 6;i++){
			rnd = Math.floor((Math.random() * (500)) + 130);
			b = new Minion(this.game, rnd, 130,'left');
			console.log(rnd);
   			this.MinionGroup.add(b);
   		}
*/
}

Monster.prototype.className = function()
{
  return "Monster";
}
