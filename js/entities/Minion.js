Minion = function(_game, _x, _y, _direction)
{
  console.log("Creating Minion!");
  
	this.game = _game;
	
	this.direction = _direction;
	
	Phaser.Sprite.call(this, this.game, _x, _y, 'Minion');

	this.enableBody = true;
	
	this.anchor.setTo(0.5,0);
	
	this.animations.add('walk_right',[6,7,8],5, true);
	this.animations.add('walk_left',[3,4,5],5, true);
	
	this.animations.play('walk_right');

	this.collideWorldBounds = true;
	
	this.checkWorldBounds = true;
	
	this.outOfBoundsKill = true;
	
	this.width = 23;
	
	this.height = 30;
	
	minion = this;
	
	this.pace = 60;
	
	this.events.onKilled.add(function(){
		Exp = Exp + 10;
		ExpText.setText("Exp " + Exp);
	}, this);
	
	//this.game.time.events.repeat(Phaser.Timer.SECOND * 3, 10, this.moveDroid, this.game);
};

Minion.prototype = Object.create(Phaser.Sprite.prototype);
Minion.prototype.constructor = Minion;

Minion.prototype.update = function() {
	
	// --------------------------------- Minion Movements
	if(this.direction == 'right'){
		this.body.velocity.x = this.pace;
		this.animations.play('walk_right');
	}
	else {
		this.body.velocity.x = -this.pace;
		this.animations.play('walk_left');
	}
	
	if(this.body.blocked.left){
		this.direction = "right";
	}
	
	if(this.body.blocked.right){
		this.direction = "left";
	}
	
/* 	console.log(Phaser.Math.roundTo((this.tileX() - 1),0)); */
	
/*
	if(Phaser.Math.roundTo(this.tileX()) != 0){
		if(map.hasTile(Phaser.Math.roundTo(this.tileX() - 1,0),this.tileY()) == false){
			this.direction = "left";
		}else if(map.hasTile(Phaser.Math.roundTo((this.tileX()) - 1,0), this.tileY()) == false){
			this.direction = "right";
		}
	}
*/
		this.sprite = (this.x - (this.width / 2))/64;
		if(map.hasTile(Phaser.Math.roundTo(this.sprite,0), this.tileY()) == false){
			this.direction = "left";
		}else if(map.hasTile(Phaser.Math.roundTo(this.sprite - 0.7,0), this.tileY()) == false){
			this.direction = "right";
		}
};


Minion.prototype.moveDroid = function () {
    //randomise the movement
    droidmover = this.rnd.integerInRange(1,3);
    //simple if statement to choose if and which way the droid moves
    //console.log(minion);
    if (droidmover == 1) {
        minion.body.velocity.x = 100;
        minion.animations.play('walk_right');
    } else if(droidmover == 2) {
        minion.body.velocity.x = -100;
        minion.animations.play('walk_right');
    } else {
        minion.body.velocity.x = 0;
        minion.animations.stop('walk_right');
    }
};

Minion.prototype.className = function()
{
  return "Minion";
};

Minion.prototype.tileX = function()
{
  return this.x/64;
};

Minion.prototype.tileY = function()
{
  return Phaser.Math.floorTo(this.y/64,0) + 1;
};

