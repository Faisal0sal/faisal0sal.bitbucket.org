Melee = function(_game, _group)
{
  console.log("Creating Melee!");
  
  	this.game = _game;
  	this.MeleeGroup = _group;
  	this.ScaleIs = 1;
	for(var i=0; i < 1; i++){
		//var Melee = this.game.add.sprite(player.x, player.y, 'hit');
		Phaser.Sprite.call(this, this.game, player.x, player.y, 'hit');
		this.MeleeGroup.add(this);
		this.body.allowGravity = false;
		this.anchor.setTo(0.5, 0.5);
		this.kill();
		this.alpha = 0.5;
		var animation = this.animations.add('hitit', [0,1,2], 8, false);
	    animation.killOnComplete = true;
	    animation.enableUpdate = true;
	}
};

Melee.prototype = Object.create(Phaser.Sprite.prototype);
Melee.prototype.constructor = Melee;

Melee.prototype.update = function() {
	
	if (cursors.left.isDown){
		this.ScaleIs = -1;
	}
	else if (cursors.right.isDown){
		this.ScaleIs = 1;
	}
};

Melee.prototype.Hit = function() {
	var Melee = this.MeleeGroup.getFirstDead();
	var tween = this.game.add.tween(Melee);
	if (Melee === null || Melee === undefined) return;
	Melee.scale.x = this.ScaleIs;
	Melee.revive();
	Melee.x = player.x + 0;
	Melee.y = player.y - 33;
	Melee.animations.play('hitit');
	if(this.ScaleIs == 1){
		tween.to({ x:Melee.x + 50 }, 200);
	}else{
		tween.to({ x:Melee.x - 50 }, 200);
	}
	
	tween.start();
};

Melee.prototype.className = function()
{
  return "Melee";
}
