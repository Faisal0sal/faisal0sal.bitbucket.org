
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {

		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar

		this.background = this.add.tileSprite(0, 0, 704, 512, 'preloaderBackground');
		this.preloadBar = this.add.sprite(300, 400, 'preloaderBar');

  		this.backgroundColor = 0xffffff;

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.

		this.load.setPreloadSprite(this.preloadBar);

		//	Here we load the rest of the assets our game needs.
		//	You can find all of these assets in the Phaser Examples repository

	    // e.g. this.load.image('image-name', 'assets/sprites/sprite.png');
	    this.load.spritesheet('random-sprite', 'assets/sprites/bar.png');
	    this.load.spritesheet('background', 'assets/preloader/background1.png');
	    this.load.spritesheet('button', 'assets/preloader/button.png');
		this.load.tilemap('level', 'assets/map_map.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.image('level_tiles', 'assets/tiles_gold.png');
		this.load.spritesheet('Minion', 'assets/enemy.png', 40, 56, 12);
		this.load.spritesheet('Monster', 'assets/Monster0_3x4.png', 298, 343, 10);
		this.load.spritesheet('fire', 'assets/fire.png',50, 17, 2);
		this.game.load.spritesheet('hit', 'assets/hit.png', 53, 60, 3);
		this.game.load.spritesheet('explosion', 'assets/explosion.png', 128, 128);
		this.load.spritesheet('AfroFaris', 'assets/AfroFaris.png', 64, 64, 14); 
		this.game.load.audio('jump', ['assets/sounds/jump.wav']);
		this.game.load.audio('die', ['assets/sounds/die.wav']);
		

	},

	create: function () {

        console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
