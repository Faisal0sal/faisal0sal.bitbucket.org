
BasicGame.MainMenu = function (game) {

	this.playButton = null;

};

BasicGame.MainMenu.prototype = {

	create: function () {

        console.log("Creating menu state!");
        
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.maxWidth = 704;
		this.scale.maxHeight = 512;
		this.scale.forceLandscape = true;
		this.scale.pageAlignHorizontally = true;
		this.scale.setScreenSize(true);
		
		this.background = this.add.tileSprite(0, 0, 704, 512, 'preloaderBackground');
		this.preloadBar = this.add.sprite(704, 512, 'background');
		//this.state.start('Game');
		// Add some buttons
		this.shadow = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'button');
		this.shadow.anchor.set(0.5);
		this.shadow.tint = 0x000000;
		this.shadow.alpha = 0.3;
		this.shadow.height = 63;
		this.shadow.width = 177;
		
		button = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'button', this.actionOnClick, this);
		button.height = 60;
		button.width = 174;
		button.inputEnabled = true;
		button.input.useHandCursor = true;

		//  setting the anchor to the center
		button.anchor.setTo(0.5, 0.5);
		
		space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    
	},

	update: function () {

		//	Do some nice funky main menu effect here
		if(space.isDown){
			console.log("Enter!.");
			this.state.start('Game');
		}

	},
	actionOnClick: function() {
		console.log('Start!.');
		this.state.start('Game');
		
	},

	resize: function (width, height) {
		//	If the game container is resized this function will be called automatically.
		//	You can use it to align sprites that should be fixed in place and other responsive display things.

	}

};
