Bullet = function(_game,_group,_explosion)
{
  console.log("Creating Bullet!");
  	
  	
  	this.game = _game;
  	this.bulletPool = _group;
	
	for(var i=0; i < 1; i++){
		Phaser.Sprite.call(this, this.game, 90, 0, 'fire');
		this.bulletPool.add(this);
		this.animations.add('fire',[0,1],30,true); 
		this.anchor.setTo(0.5, 0);
		this.game.physics.enable(this, Phaser.Physics.ARCADE);
		//this.body.setSize(50, 17, 0, -1);
		this.kill();
	}
  	
  	this.GRAVITY = 500;
	this.SHOT_DELAY = 100; // milliseconds (10 bullets/second)
	this.BULLET_SPEED = 430; // pixels/second
  	this.BULLET_ROTATION = -1.1;
  	
  	this.explosionGroup = _explosion;
  	// Setup a canvas to draw the trajectory on the screen
    this.bitmap = this.game.add.bitmapData(map.widthInPixels, map.heightInPixels);
    this.bitmap.context.fillStyle = 'rgb(255, 255, 255)';
    this.bitmap.context.strokeStyle = 'rgb(255, 255, 255)';
    this.game.add.image(0, 0, this.bitmap);
};

Bullet.prototype = Object.create(Phaser.Sprite.prototype);
Bullet.prototype.constructor = Bullet;


Bullet.prototype.update = function() {
	
	this.bulletPool.forEachAlive(function(bullet) {
        bullet.rotation = Math.atan2(bullet.body.velocity.y, bullet.body.velocity.x);
	}, this);
    	
	if (cursors.left.isDown){
		this.BULLET_ROTATION = -2.05;
	}
	else if (cursors.right.isDown){
		this.BULLET_ROTATION = -1.1;
	}
	
	// ---------------------------------- Draw trajectory
	if(Ability == 'Fire'){
		this.drawTrajectory();
	}else{
		this.bitmap.clear();
		//this.bitmap.context.fillStyle = 'rgb(0,0,0)';
	}
	// ---------------------------------- Explosion
	this.Explosion();
};

Bullet.prototype.fire = function() {
	
      if (this.lastBulletShotAt === undefined) this.lastBulletShotAt = 0;
      if (this.game.time.now - this.lastBulletShotAt < this.SHOT_DELAY) return;
      this.lastBulletShotAt = this.game.time.now;
      var bullet = this.bulletPool.getFirstDead();
      if (bullet === null || bullet === undefined) return;
      bullet.revive();
      this.animations.play('fire');
      bullet.checkWorldBounds = true;
      bullet.outOfBoundsKill = true;
	  x = player.x + 0;
	  y = player.y - 40;
	  bullet.reset(x, y);
	  bullet.rotation = this.BULLET_ROTATION;
    
	  // Shoot it
      bullet.body.velocity.x = Math.cos(bullet.rotation) * this.BULLET_SPEED;
      bullet.body.velocity.y = Math.sin(bullet.rotation) * this.BULLET_SPEED;
	
}

Bullet.prototype.drawTrajectory = function() {
    
	this.bitmap.context.clearRect(0, 0, this.game.width + player.x, this.game.height + player.y);
	this.bitmap.context.fillStyle = 'rgb(255, 255, 255)';
	var MARCH_SPEED = 40; // Smaller is faster
	this.timeOffset = this.timeOffset + 1 || 0;
	this.timeOffset = this.timeOffset % MARCH_SPEED;
	var correctionFactor = 0.99;
	// Draw the trajectory
	// http://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29
	var theta = -this.BULLET_ROTATION;
	var x = 0, y = 0;
	for(var t = 0 + this.timeOffset/(1000*MARCH_SPEED/60); t < 3; t += 0.03) {
    	x = this.BULLET_SPEED * t * Math.cos(theta) * correctionFactor;
		y = this.BULLET_SPEED * t * Math.sin(theta) * correctionFactor - 0.5 * this.GRAVITY * t * t + 19;
		this.bitmap.context.fillRect(x + player.x ,player.y - y - 13, 2, 1);
		if (y < 0) break;
  	}
  	this.bitmap.dirty = true;
}

Bullet.prototype.Explosion = function() {
	
	this.game.physics.arcade.collide(this.bulletPool, layer, function(bullet, layer) {
		    
		      var explosion = this.explosionGroup.getFirstDead();
		      if (explosion === null) {
		          explosion = this.game.add.sprite(0, 0, 'explosion');
		          explosion.anchor.setTo(0.5, 0.5);
		          var animation = explosion.animations.add('boom', [0,1,2,3], 60, false);
		          animation.killOnComplete = true;
		          this.explosionGroup.add(explosion);
		          explosion.body.allowGravity = false;
		      }
		      explosion.revive();
		      explosion.x = bullet.x;
		      explosion.y = bullet.y;
		      explosion.angle = this.game.rnd.integerInRange(0, 360);
		      explosion.animations.play('boom');
        bullet.kill();
    }, null, this);
    
    this.game.physics.arcade.collide(this.bulletPool, monster, function(body1, body2) {
		    
		      var explosion = this.explosionGroup.getFirstDead();
		      if (explosion === null) {
		          explosion = this.game.add.sprite(0, 0, 'explosion');
		          explosion.anchor.setTo(0.5, 0.5);
		          var animation = explosion.animations.add('boom', [0,1,2,3], 60, false);
		          animation.killOnComplete = true;
		          this.explosionGroup.add(explosion);
		          explosion.body.allowGravity = false;
		      }
		      explosion.revive();
		      explosion.x = bullet.x;
		      explosion.y = bullet.y;
		      explosion.angle = this.game.rnd.integerInRange(0, 360);
		      explosion.animations.play('boom');
        bullet.kill();
    }, null, this);
	
}

Bullet.prototype.className = function()
{
  return "Bullet";
}
