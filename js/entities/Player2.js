Player2 = function(_game, _x, _y)
{
  console.log("Creating Player 2 !");
  
  this.game = _game;
  
  Phaser.Sprite.call(this, this.game, _x, _y, 'AfroFaris');
  
  this.game.add.existing(this);
  
  this.game.physics.arcade.enable([this]); 
  
  this.game.physics.arcade.gravity.y = 500; 
  
  this.collideWorldBounds = true;
  
  this.checkWorldBounds = true;
  
  this.outOfBoundsKill = true;
  
  this.body.setSize(30, 64, 0, -1);
  
  this.anchor.setTo(0.5, 1);
  
  this.game.camera.follow(this, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT); 
  
  cursors = this.game.input.keyboard.createCursorKeys();
  space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  // ------------------------------- Animation
  
  this.animations.add('run',[0,1,2],10,true); 
  
  this.animations.add('stand',[6]);
  
  this.animations.add('fire',[4],10,true);
  
  // --------------------------------- Play sound
  
  jump = this.game.add.audio('jump');
  
  die = this.game.add.audio('die');
  
      
  this.events.onKilled.add(function(){
		this.RestartMenu();
		die.play();
	}, this);
  return this;
};

Player2.prototype = Object.create(Phaser.Sprite.prototype);
Player2.prototype.constructor = Player2;

Player2.prototype.update = function() {
	
	// --------------------------------- Player Movements
	if (cursors.left.isDown){
		player2.body.velocity.x = -200;
		player2.animations.play('run');
		player2.scale.x = -1;
	}
	else if (cursors.right.isDown){
		player2.body.velocity.x = 200;
		player2.animations.play('run');
		player2.scale.x = 1;
	}
	else{
		player2.body.velocity.x = 0;
		player2.animations.play('stand');
	}
	if (cursors.up.isDown && player2.body.onFloor()){
		player2.body.velocity.y = -390;
		jump.play();
	}
	
	
	if (space.isDown)
    {
	    this.Fire();
    }
};

Player2.prototype.RestartMenu = function() {
	//
	label = this.game.add.text(player.x -30 , this.game.height / 2, '',{ font: '22px Lucida Console', fill: '#fff', align: 'center'});
	label.text ='\nGAME OVER\nPress SPACE to restart';
	label.anchor.setTo(0.5, 0.5);
	label.fixedToCamera = true;
    label.cameraOffset.setTo(this.game.camera.width / 2, this.game.camera.height / 2);
};

Player2.prototype.Fire = function() {
		player2.animations.play('fire');
		if(player2.alive == false)
		this.game.state.start("Game", true, false);
}

Player2.prototype.className = function()
{
  return "Player2";
}
