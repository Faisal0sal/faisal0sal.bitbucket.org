Player = function(_game, _x, _y)
{
  console.log("Creating Player!");
  
  this.game = _game;
  
  Phaser.Sprite.call(this, this.game, _x, _y, 'AfroFaris');
  
  this.game.add.existing(this);
  
  this.game.physics.arcade.enable([this]); 
  
  this.game.physics.arcade.gravity.y = 500; 
  
  this.collideWorldBounds = true;
  
  this.checkWorldBounds = true;
  
  this.outOfBoundsKill = true;
  
  this.body.setSize(30, 64, 0, -1);
  
  this.anchor.setTo(0.5, 1);
  
  this.game.camera.follow(this, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT); 
  
  cursors = this.game.input.keyboard.createCursorKeys();
  space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  switchButton = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
  // ------------------------------- Animation
  
  this.animations.add('run',[0,1,2],10,true); 
  
  this.animations.add('stand',[6]);
  
  this.animations.add('jump',[0]); 
    
  this.animations.add('fire',[4],10,true);
  
  // --------------------------------- Play sound
  
  jump = this.game.add.audio('jump');
  
  die = this.game.add.audio('die');

  this.game.input.gamepad.start();
  
  pad = this.game.input.gamepad.pad1;
  pad.addCallbacks(this, { onConnect: this.addButtons });
      
  this.events.onKilled.add(function(){
		this.RestartMenu();
		die.play();
	}, this);
  return this;
};

Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

Player.prototype.update = function() {
	
	// --------------------------------- Player Movements
	if (cursors.left.isDown){
		player.body.velocity.x = -200;
		player.animations.play('run');
		player.scale.x = -1;
	}
	else if (cursors.right.isDown){
		player.body.velocity.x = 200;
		player.animations.play('run');
		player.scale.x = 1;
	}
	else{
		player.body.velocity.x = 0;
		player.animations.play('stand');
	}
	if (cursors.up.isDown && player.body.onFloor()){
        player.animations.play('jump');
		player.body.velocity.y = -390;
		jump.play();
	}
    
    if (!player.body.onFloor())
	   player.animations.play('jump');
	
	if (space.isDown)
    {
	    this.Fire();
    }
};

Player.prototype.RestartMenu = function() {
	//
	label = this.game.add.text(player.x -30 , this.game.height / 2, '',{ font: '22px Lucida Console', fill: '#fff', align: 'center'});
	label.text ='\nGAME OVER\nPress SPACE to restart';
	label.anchor.setTo(0.5, 0.5);
	label.fixedToCamera = true;
    label.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 0);
    label.cameraOffset.setTo(this.game.camera.width / 2, this.game.camera.height / 2);
};

Player.prototype.Fire = function() {
		player.animations.play('fire');
		if(player.alive == false)
		this.game.state.start("Game", true, false);
}

Player.prototype.addButtons = function() {
	
	console.log('Gamepad is set');
	//  We can't do this until we know that the gamepad has been connected and is started

    buttonA = pad.getButton(Phaser.Gamepad.XBOX360_A);
    buttonX = pad.getButton(Phaser.Gamepad.XBOX360_X);
    
    buttonA.onDown.add( function() { space.isDown = true } ,this);
    buttonA.onUp.add( function() { space.isDown = false}, this);
    
    buttonX.onDown.add( function() { switchButton.isDown = true } ,this);
    buttonX.onUp.add( function() { switchButton.isDown = false}, this);
    //  These won't work in Firefox, sorry! It uses totally different button mappings

    buttonDPadLeft = pad.getButton(Phaser.Gamepad.XBOX360_DPAD_LEFT);
    buttonDPadRight = pad.getButton(Phaser.Gamepad.XBOX360_DPAD_RIGHT);
    buttonDPadUp = pad.getButton(Phaser.Gamepad.XBOX360_DPAD_UP);
    
    buttonDPadLeft.onDown.add( function() { cursors.left.isDown = true } ,this);
    buttonDPadLeft.onUp.add( function() { cursors.left.isDown = false}, this);
    
    buttonDPadRight.onDown.add( function() { cursors.right.isDown = true } ,this);
    buttonDPadRight.onUp.add( function() { cursors.right.isDown = false}, this);
    
    buttonDPadUp.onDown.add( function() { cursors.up.isDown = true } ,this);
    buttonDPadUp.onUp.add( function() { cursors.up.isDown = false}, this);
	
}

Player.prototype.className = function()
{
  return "Minion";
}
