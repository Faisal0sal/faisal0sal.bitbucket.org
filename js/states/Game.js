
BasicGame.Game = function (game) {

	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, s, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;    //  the tween manager
    this.state;	    //	the state manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator
    
    this.map;
    this.layer;
    this.player;
    this.cursors;

    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

};
	
BasicGame.Game.prototype = {


	create: function () {
        console.log("Creating game state!");
        // Scale
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.maxWidth = 704;
		this.scale.maxHeight = 512;
		this.scale.forceLandscape = true;
		this.scale.pageAlignHorizontally = true;
		this.scale.setScreenSize(true);
        //Change the stage background colour to classic Mario blue.7ec0ee
        this.stage.backgroundColor = '#FCEBB6';
        
        
        //Populate the local map attribute with the tilemap with the key “level”
        map = this.add.tilemap('level'); 
        map.addTilesetImage('terrain', 'level_tiles');
        // gradient background
        var myBitmap = this.game.add.bitmapData(map.widthInPixels, map.heightInPixels);
		var grd = myBitmap.context.createLinearGradient(0,0,0,map.heightInPixels);
		grd.addColorStop(0,"#FCEBB6");
		grd.addColorStop(500/600,"#0a68b0");
		grd.addColorStop(580/600,"#0a68b0");
		grd.addColorStop(1,"black");
		myBitmap.context.fillStyle = grd;
		myBitmap.context.fillRect(0,0,map.widthInPixels,map.heightInPixels);
		this.game.add.sprite(0, 0, myBitmap);
		
        //Assign the “level_tiles” tileset to the tilemap data
        layer = map.createLayer('terrain');
        //Populate the local layer with the combined level data.
        layer.resizeWorld();
		map.setCollision([15,16,17,18,19,20,25,26,27,29,30,35,36,37,39,40], true);
        //Resize the game world to fit the level data.
        this.physics.startSystem(Phaser.Physics.ARCADE);
        // --------------------------------- Add Player
        player = new Player(this.game, 190, 1730);
        //player2 = new Player2(this.game, 170, 1730);
        
        // --------------------------------- Add Monster
        monster = new Monster(this.game ,2200, 1250);
        
        
        MinionGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 2, this.ThrowMinions, this);
        
		// set Default Attack
		Ability = 'Melee';
		// set Experience
		Exp = 0;
		// Gamepad
		// -------------------------------- Add Enemy
		this.MinionGroup = this.game.add.group();
		this.MinionGroup.enableBody = true;
		this.MinionGroup.physicsBodyType = Phaser.Physics.ARCADE;
   		
   		g = new Minion(this.game, 700, 1700, 'right');
   		a = new Minion(this.game, 1180, 1250, 'right');
   		b = new Minion(this.game, 900, 1500, 'right');
   		c = new Minion(this.game, 900, 1200, 'right');
   		d = new Minion(this.game, 900, 800, 'right');
   		e = new Minion(this.game, 700, 800, 'right');
   		f = new Minion(this.game, 900, 1200, 'right');
   			
   			this.MinionGroup.add(g);
   			this.MinionGroup.add(a);
   			this.MinionGroup.add(c);
   			this.MinionGroup.add(d);
   			this.MinionGroup.add(e);
   			this.MinionGroup.add(f);
   			this.MinionGroup.add(b);
		// -------------------------------- Add bullets
    	// Create an object pool of bullets
    	this.bulletPool = this.game.add.group();
    	this.explosionGroup = this.game.add.group();
	   	this.explosionGroup.enableBody = true;
	   	this.explosionGroup.physicsBodyType = Phaser.Physics.ARCADE;
   		bullet = new Bullet(this.game,this.bulletPool,this.explosionGroup);
   		
		// Game over
		//over = new Over(this.game);
		
/*
		if(map.hasTile(30,25)==false){
			
		}
*/
		//console.log(map.width);
		//console.log(map.tileWidth);
		//console.log(map.widthInPixels);
		//console.log(player.x)
		
		
		this.MeleeGroup = this.game.add.group();
		this.MeleeGroup.enableBody = true;
		melee = new Melee(this.game,this.MeleeGroup);
	   	
		// Show FPS
		this.game.time.advancedTiming = true;
		this.fpsText = this.game.add.text(200, 200, 'Press Space to attack', { font: '16px Arial', fill: '#ffffff' }
      );
      
        this.fpsText = this.game.add.text(700, 200, 'Press Z to switch to Fire', { font: '16px Arial', fill: '#ffffff' }
      );
      	
      	WeaponText = this.game.add.text(20, 20, 'Melee', { font: '16px Arial', fill: '#ffffff' });
      	WeaponText.fixedToCamera = true;
      	WeaponText.cameraOffset.setTo(20,20);
      	WeaponText.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 0);
        
        this.FirstInstruction = this.game.add.text(240, 1430, 'Arrows to move and jump, Space to attack ', { font: '16px Arial', fill: '#F5F538' });
        this.FirstInstruction.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 2);
        this.SecondInstruction = this.game.add.text(750, 1390, 'Killing a minion is 10 exp, 50 exp to unlock a new ability', { font: '16px Arial', fill: '#F5F538' });
        this.SecondInstruction.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 2);
        this.ThirdInstruction = this.game.add.text(1900, 1390, 'Our melee attack is too weak for a monter, use Fire instead.', { font: '16px Arial', fill: '#F5F538' });
        this.ThirdInstruction.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 2);
        
        UnlockedFire = this.game.add.text(20, 20, 'Fire is Unlocked, to switch ability press Z', { font: '16px Arial', fill: '#F5F538' });
        UnlockedFire.fixedToCamera = true;
        UnlockedFire.cameraOffset.setTo(260,20);
        UnlockedFire.visible = false;
        UnlockedFire.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 0);
        
      	ExpText = this.game.add.text(20, 20, 'Exp ' + Exp, { font: '16px Arial', fill: '#ffffff' });
      	ExpText.fixedToCamera = true;
      	ExpText.cameraOffset.setTo(20,45);
        ExpText.setShadow(0, 2, 'rgba(0, 0, 0, 1)', 0);
      	
      	cursors = this.game.input.keyboard.createCursorKeys();
      	switchButton = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
      	fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
      	this.game.world.bringToTop(player);
      	this.game.world.bringToTop(this.bulletPool);
      	
      	shakeWorld = 0;
      	this.currentBounds = this.game.world.bounds;
        isVisible = true;
		//	Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!
		
	},

	
	update: function () {
        
        this.Switch();
		
		if (shakeWorld > 0) {
		   var rand1 = this.game.rnd.integerInRange(-10,10);
		   var rand2 = this.game.rnd.integerInRange(-10,10);
		   this.currentBoundsWidth = this.game.world.bounds.width;
		   this.currentBoundsHeight = this.game.world.bounds.height;
		   
		   this.game.world.setBounds(rand1, 
		    							rand2, 
		    								this.game.world.bounds.width + rand1, 
		    									this.game.world.bounds.height + rand2);
		    shakeWorld--;
		        if (shakeWorld == 0) {
					this.game.world.setBounds(0,0,
				    								map.widthInPixels,
				    									map.heightInPixels);
		    	}
		}
		//	Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!
	  	this.MinionGroup.forEachAlive(this.updateGroup,this);
	  	// --------------------------------- Enable Physics
	  	this.game.physics.arcade.collide(player, layer);
	  	//this.game.physics.arcade.collide(player2, layer);
		this.game.physics.arcade.collide(this.MinionGroup, layer);
		this.game.physics.arcade.collide(monster, layer);
		
		//console.log("x:" + player.x);
		//console.log("y:" + player.y);
		// ------------------------------------------ Fire
		if (fireButton.isDown){
			if(Ability == 'Melee'){
				melee.Hit();
			}else{
				bullet.fire();
			}
		}
		
		Monsterhealth.setText("Health " + monster.health);
		
		// Explosion
    	this.physics.arcade.overlap(this.MinionGroup, this.explosionGroup, function(body1, body2) {
		    console.log("explosion collide");
		    body1.kill();
	    }, null, this);
	    
	    // Melee
    	this.physics.arcade.overlap(this.MinionGroup, this.MeleeGroup, function(body1, body2) {
		    console.log("Melee collide");
		    body1.kill();
	    }, null, this);
	    
	    // Minion kill player
	    this.physics.arcade.overlap(this.MinionGroup, player, function(body1, body2) {
		    console.log("player collide");
		    body1.kill();
	    }, null, this);
	    
	    // Monster kill player
	    this.physics.arcade.overlap(monster, player, function(body1, body2) {
		    console.log("player collide");
		    body2.kill();
	    }, null, this);
	    
	    this.physics.arcade.overlap(monster, this.explosionGroup, function(body1, body2) {
		    console.log("explosion collide");
		    body1.damage(1);
		    shakeWorld = 5;
	    }, null, this);

	},
    render: function (){
	        //this.game.debug.body(g);
	        //this.explosionGroup.forEachAlive(this.renderGroup,this);
	        
/*
	        this.game.debug.text(layer.getTileX(this.game.input.activePointer.worldX), 32, 128, '#000000');
			this.game.debug.text(layer.getTileY(this.game.input.activePointer.worldY), 32, 148, '#000000');
*/
	        
    },
    renderGroup: function(sprite){
		this.game.debug.body(sprite);
		//layer.debug = true;
	},
	MinionGroup: function(sprite){
		sprite.update(); 
	},
    ThrowMinions: function(){
	    if(monster.inCamera){
		    q = new Minion(this.game, monster.x, monster.y + 240,'left');
   			this.MinionGroup.add(q);
   			Monsterhealth.fixedToCamera = true;
   			Monsterhealth.cameraOffset.setTo(this.game.camera.width / 2, this.game.camera.height / 2);
   		}else{
	   		console.log("Not on camera");
   		}
	},
	Switch: function(){
		        if (Exp >= 50){
            
            UnlockedFire.visible = true;
            if (isVisible == false){
                UnlockedFire.visible = false;
            }
            if(switchButton.isDown){
                
                isVisible = false;
                if(Ability == 'Melee'){
                    Ability = 'Fire';
                } else {
                    Ability = 'Melee';
                }
                switchButton.isDown = false;
                WeaponText.setText(Ability);
            }
        }
	},
	quitGame: function (pointer) {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		this.state.start('MainMenu');

	}

};
